const colors = require('colors');
const { getData } = require('./getData');

module.exports.getCollectionId = function getCollectionId() {
    process.stdin.setEncoding('utf-8');
    return new Promise(async (resolve, reject) => {
        let page = 1;
        let search = '';

        display(page);

        process.stdin.on('data', data => {
            if (data === 'exit\n') {
                reject(new Error('No input received'));
            } else if (data.trim().match(/^[0-9]+$/)) {
                // incrememnt page and redisplay if we were given a number
                // (persist the current search)
                page = data;
                display(page, search);
            } else if (data.trim() === 'n') {
                page++;
                display(page, search);
            } else if (data.trim() === 'p') {
                page--;
                display(page, search);
            } else if (data.trim().match(/^[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12}$/)) {
                // resolve if we were given a uuid
                resolve(data);
            } else {
                // otherwise treat all input as a search
                display(page, data);
            }
        });
    });
};

function helpText() {
    console.log(
        `Look up or enter a collection id. \n` +
            ` - Type ` +
            colors.bold.underline('n') +
            ` or ` +
            colors.bold.underline('p') +
            ` to navigate through pages \n` +
            ` - Type a page number to go to that page \n` +
            ` - Type a search term to look up collections \n` +
            ` - Or enter a collection ID to finish. \n` +
            ` - Type ` +
            colors.bold.underline('exit') +
            ` to quit the program \n` +
            `---------------------------------------------\n `
    );
}

async function display(page, search = '') {
    console.log('fetching page ' + page);

    if (search.trim() !== '') {
        console.log('searching for ' + colors.underline(search));
    }

    const endpoint = `http://api.repo.nypl.org/api/v1/collections`;
    let data;

    try {
        data = await getData(
            `${endpoint}?page=${page}` +
            (search.trim() !== '' ? `&q=${encodeURIComponent(search)}` : '')
        );
    } catch (error) {
        console.error(error.message);
    }

    helpText();

    if (data.collection === undefined || data.collection.length === 0) {
        console.log('No items!'.red);
        return;
    }

    for (let index = 0, num = data.collection.length; index < num; index++) {
        let item = data.collection[index];
        console.log(
            `${index + (data.perPage * data.page - data.perPage) + 1}. ` +
                `${item.title} (${item.numItems} items): ${item.uuid}\r`
        );
    }
}