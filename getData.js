const fetch = require('node-fetch');
const fs = require('fs');
const path = require('path');

module.exports.getData = async url => {
    let data;

    if (url.indexOf('?') === -1) {
        url = `${url}?`;
    } else {
        url = `${url}&`;
    }

    if (url.indexOf('per_page') === -1) {
        url = `${url}per_page=20&`;
    }

    url = `${url}publicDomainOnly=false`;

    const response = await fetch(url, {
        headers: {
            Authorization: 'Token token="b8utulnwbdmf4y03"'
        }
    });
    data = await response.json();

    if (data.nyplAPI.response.headers.message !== 'ok') {
        throw new Error(JSON.stringify(data.nyplAPI.response.headers.message));
    }

    return data.nyplAPI.response;
};

module.exports.getImage = async (url, destination) => {
    const pathParts = destination.split('/');
    const filename = pathParts.pop();
    const folderName = path.join(...pathParts);
    let folders = [];

    for (let folder of pathParts) {
        folders.push(folder);
        if (!fs.existsSync(path.join(...folders))) {
            fs.mkdirSync(path.join(...folders));
        }
    }

    const response = await fetch(url);
    const dest = fs.createWriteStream(path.join(...[folderName, filename]));
    response.body.pipe(dest);

    console.log('Downloaded ' + filename);
};
