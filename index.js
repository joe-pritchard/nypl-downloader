const colors = require('colors');
const { getCollectionId } = require('./getCollectionId.js');
const { getData, getImage } = require('./getData');

getCollectionId()
    .then(async collectionId => {
        const items = await getItemsFromCollection(collectionId);

        for (let index = 0, num = items.length; index < num; index++) {
            let item = items[index];
            const data = await getData(`http://api.repo.nypl.org/api/v1/items/${item.uuid}.json?withTitles=yes`);

            let captures = data.capture;

            for (let capture of captures) {
                if (
                    capture.imageLinks !== null &&
                    capture.imageLinks.imageLink !== undefined &&
                    capture.imageLinks.imageLink.length > 0
                ) {
                    let image = capture.imageLinks.imageLink.find(c => c.match(/t=r/));
                    if (image !== undefined) {
                        getImage(image, `./nypl-downloads/${collectionId.trim()}.${item.uuid}.${capture.uuid}.jpeg`);
                    }
                } else {
                    console.log('no images found for ' + item.uuid);
                }
            }
        }

        console.log('job done');
        process.exit();
    })
    .catch(error => {
        console.error(error.message);
        process.exit();
    });

async function getItemsFromCollection(collectionId, page = 1) {
    console.log('Fetching image data from collection id '.green + colors.bold.green(collectionId));
    const endpoint = `http://api.repo.nypl.org/api/v1/collections/${collectionId}?per_page=500&page=${page}`;
    const data = await getData(endpoint);
    let items = [];
    let subItems = [];

    if (data.hasOwnProperty('collection')) {
        for (let collectionIndex = 0, len = data.collection.length; collectionIndex < len; collectionIndex++) {
            subItems = await getItemsFromCollection(data.collection[collectionIndex].uuid);
            items = items.concat(subItems);
        }
    }

    if (data.hasOwnProperty('item')) {
        if (data.numItems > 1) {
            console.log('fetching '.green + colors.bold.green(`${data.item.length * page} of ${data.numItems}`) + ' items from collection'.green);
            for (let itemIndex = 0, len = data.item.length; itemIndex < len; itemIndex++) {
                items = items.concat(data.item);
            }

            if (data.numItems > data.item.length * page) {
                items = items.concat(await getItemsFromCollection(collectionId, page + 1));
            }
        } else {
            console.log('fetching 1 item from collection'.green);
            items.push(data.item);
        }
    }

    return items;
}